import { Observable, of } from 'rxjs';

export const ApiServiceMock = {
  getEmailsByType: jest.fn().mockImplementation(() => {
    const mockApi = [
      {
        id: 1,
        from : {
          name : 'Jese Test',
          email : 'test@test.com'
        },
        subject : 'test',
        body : 'test',
        read: false,
        datetime: 'Jan 27, 2019',
        type: 'receipt'
      }
    ];
    const mockApiPromise: Observable<any> = of(mockApi);
    return mockApiPromise;
  }),
};
